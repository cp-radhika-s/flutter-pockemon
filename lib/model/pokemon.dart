import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pokedex_flutter/model/colors.dart';

part 'pokemon.g.dart';

@JsonSerializable(createToJson: false)
class Pokemon {
  const Pokemon({
    required this.xdescription,
    required this.ydescription,
    required this.category,
    required this.weaknesses,
    required this.id,
    required this.name,
    required this.types,
    required this.image,
    required this.height,
    required this.weight,
    required this.eggGroups,
    required this.evolutions,
    required this.reason,
  });

  final String id;
  final String name;
  @JsonKey(name: "imageurl")
  final String image;
  final String xdescription;
  final String ydescription;
  final String category;
  final String height;
  final String weight;
  @JsonKey(name: "egg_groups")
  final String eggGroups;
  @JsonKey(name: "typeofpokemon")
  final List<String> types;
  @JsonKey(name: "weaknesses")
  final List<String> weaknesses;
  final List<String> evolutions;
  final String reason;

  Color get color => PokedexColor.lightGreen;

  factory Pokemon.fromJson(Map<String, dynamic> json) =>
      _$PokemonFromJson(json);
}
