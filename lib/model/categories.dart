
import 'package:flutter/material.dart';
import 'package:pokedex_flutter/model/colors.dart';

class Category {
  const Category({
    required this.name,
    required this.color
  });

  final Color color;
  final String name;
}

const List<Category> categories = [
  Category(name: 'Pokedex', color: PokedexColor.teal),
  Category(name: 'Moves', color: PokedexColor.red),
  Category(name: 'Abilities', color: PokedexColor.blue),
  Category(name: 'Items', color: PokedexColor.yellow),
  Category(name: 'Locations', color: PokedexColor.purple),
 ];
