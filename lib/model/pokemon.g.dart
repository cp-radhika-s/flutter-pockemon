// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pokemon _$PokemonFromJson(Map<String, dynamic> json) => Pokemon(
      xdescription: json['xdescription'] as String,
      ydescription: json['ydescription'] as String,
      category: json['category'] as String,
      weaknesses: (json['weaknesses'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      id: json['id'] as String,
      name: json['name'] as String,
      types: (json['typeofpokemon'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      image: json['imageurl'] as String,
      height: json['height'] as String,
      weight: json['weight'] as String,
      eggGroups: json['egg_groups'] as String,
      evolutions: (json['evolutions'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      reason: json['reason'] as String,
    );
