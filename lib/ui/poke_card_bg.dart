import 'package:flutter/material.dart';

class PokeCardBackground extends StatelessWidget {
  static const double _pokeballWidthFraction = 0.664;

  final Widget child;

  const PokeCardBackground({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final safeAreaTop = MediaQuery.of(context).padding.top;
    final pokeballSize =
        MediaQuery.of(context).size.width * _pokeballWidthFraction;
    final appBarHeight = AppBar().preferredSize.height;
    const iconButtonPadding = 28;
    final iconSize = IconTheme.of(context).size ?? 0;

    final pokeballTopMargin =
        -(pokeballSize / 2 - safeAreaTop - appBarHeight / 2);
    final pokeballRightMargin =
        -(pokeballSize / 2 - iconButtonPadding - iconSize / 2);

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            child: Image(
              image: AssetImage("assets/pokeball.png"),
              width: pokeballSize,
              height: pokeballSize,
              color: Colors.black.withOpacity(0.05),
            ),
            top: pokeballTopMargin,
            right: pokeballRightMargin,
          ),
          child
        ],
      ),
     );
  }
}
