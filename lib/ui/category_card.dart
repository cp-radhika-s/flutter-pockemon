import 'package:flutter/material.dart';
import 'package:pokedex_flutter/model/categories.dart';

class CategoryCard extends StatelessWidget {
  final Category category;
  final void Function()? onPress;

  const CategoryCard(
    this.category, {
    this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      final itemWidth = constrains.maxWidth;

      return Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: itemWidth * 0.82,
              height: 11,
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                    color: category.color,
                    offset: const Offset(0, 3),
                    blurRadius: 23)
              ]),
            ),
          ),
          Material(
            color: category.color,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              splashColor: Colors.white10,
              highlightColor: Colors.white10,
              onTap: onPress,
              child: Stack(
                children: [
                  Positioned(
                    bottom: -2,
                    right: -2,
                    child: Image(
                      image: const AssetImage("assets/pokeball.png"),
                      width: 50,
                      height: 50,
                      color: Colors.white.withOpacity(0.14),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Text(
                      category.name,
                      style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      );
    });
  }
}
