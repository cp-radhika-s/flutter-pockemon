import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pokedex_flutter/model/pokemon.dart';
import 'package:pokedex_flutter/ui/poke_card_bg.dart';
import 'package:pokedex_flutter/ui/pokemon_card.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PokdexListView extends StatefulWidget {
  const PokdexListView({Key? key}) : super(key: key);

  @override
  _PokdexListViewState createState() => _PokdexListViewState();
}

class _PokdexListViewState extends State<PokdexListView> {
  late Future<List<Pokemon>> _futurePokemonList;

  @override
  void initState() {
    _futurePokemonList = fetchList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PokeCardBackground(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Pokedex"),
          backgroundColor: Colors.transparent,
        ),
        body: FutureBuilder<List<Pokemon>>(
          future: _futurePokemonList,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Pokemon> items = snapshot.data!;
              return GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.fromLTRB(24, 40, 24, 60),
                itemCount: items.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  childAspectRatio: 1.57,
                  mainAxisSpacing: 15,
                ),
                itemBuilder: (context, index) {
                  return PokemonCard(items[index], onPress: () => {});
                },
              );
            } else if (snapshot.hasError) {
              print(snapshot.error.toString());
              return Text("Failed to load" + snapshot.error.toString());
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}

Future<List<Pokemon>> fetchList() async {
  final response = await http.get(Uri.parse(
      'https://gist.githubusercontent.com/hungps/0bfdd96d3ab9ee20c2e572e47c6834c7/raw/pokemons.json'));

  final data = (json.decode(response.body) as List)
      .map((item) => Pokemon.fromJson(item))
      .toList();
  return data;
}
