import 'package:flutter/material.dart';
import 'package:pokedex_flutter/model/categories.dart';
import 'package:pokedex_flutter/ui/category_card.dart';
import 'package:pokedex_flutter/ui/pokdex_list.dart';
import 'package:pokedex_flutter/ui/poke_card_bg.dart';

class HeaderCardView extends StatelessWidget {
  static const double height = 582;

  const HeaderCardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.hardEdge,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(30)),
      ),
      child: PokeCardBackground(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
                child: Container(
              constraints: const BoxConstraints.expand(),
              padding: const EdgeInsets.all(28),
              alignment: Alignment.bottomLeft,
              child: const Text(
                "What Pokemon\are you looking for?",
                style: TextStyle(
                    fontSize: 30, height: 1.6, fontWeight: FontWeight.w900),
              ),
            )),
            const SizedBox(height: 20),
            GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.fromLTRB(28, 42, 28, 62),
                itemCount: categories.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  childAspectRatio: 2.6,
                  mainAxisSpacing: 15,
                ),
                itemBuilder: (context, index) {
                  return CategoryCard(
                    categories[index],
                    onPress: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => PokdexListView()))
                    },
                  );
                })
          ],
        ),
      ),
    );
  }
}
