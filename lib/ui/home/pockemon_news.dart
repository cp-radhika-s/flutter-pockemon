import 'package:flutter/material.dart';
import 'package:pokedex_flutter/model/colors.dart';

class PokemonNews extends StatelessWidget {
  const PokemonNews({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const ClampingScrollPhysics(),
      children: [_buildHeader(context), _buildNews()],
    );
  }

  Widget _buildHeader(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(left: 28, top: 0, right: 28, bottom: 22),
      child: Text(
        'Pokemon News',
        style: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w900,
        ),
      ),
    );
  }

  Widget _buildNews() {
    return ListView.separated(
      shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 16),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text("Pokémon Rumble Rush Arrives Soon",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                      SizedBox(height: 6),
                      Text(
                        "15 May 2019",
                        style: TextStyle(
                          fontSize: 10,
                          color: PokedexColor.darkGrey,
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(width: 36),
                const Image(
                    width: 110,
                    height: 65,
                    image: AssetImage("assets/thumbnail.png"))
              ],
            ),
          );
        },
        separatorBuilder: (context, index) => const Divider(),
        itemCount: 10);
  }
}
