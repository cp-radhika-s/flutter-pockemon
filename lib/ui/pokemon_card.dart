import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_flutter/model/pokemon.dart';

class PokemonCard extends StatelessWidget {
  final Pokemon pokemon;
  final void Function()? onPress;

  const PokemonCard(
    this.pokemon, {
    this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      decoration: BoxDecoration(
          color: pokemon.color,
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: pokemon.color.withOpacity(0.4),
              blurRadius: 15,
              offset: const Offset(0, 8),
            )
          ]),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Material(
          color: pokemon.color,
          child: InkWell(
            onTap: () => {},
            splashColor: Colors.white10,
            highlightColor: Colors.white10,
            child: Stack(
              children: [
                Positioned(
                  bottom: -2,
                  right: -2,
                  child: Image(
                    image: const AssetImage("assets/pokeball.png"),
                    width: 50,
                    height: 50,
                    color: Colors.white.withOpacity(0.14),
                  ),
                ),
                Positioned(
                    bottom: -2,
                    right: 2,
                    child: CachedNetworkImage(
                      imageUrl: pokemon.image,
                      imageBuilder: (_, image) => Image(
                        image: image,
                        width: 80,
                        height: 80,
                        alignment: Alignment.bottomCenter,
                        fit: BoxFit.fill,
                      ),
                    )),
                Positioned(
                  top: 10,
                  right: 14,
                  child: Text(
                    pokemon.id,
                    style: const TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: Colors.black12,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                      padding: const EdgeInsets.fromLTRB(16, 24, 16, 26),
                      child: Text(
                        pokemon.name,
                        style: const TextStyle(
                            fontSize: 14,
                            height: 0.7,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
